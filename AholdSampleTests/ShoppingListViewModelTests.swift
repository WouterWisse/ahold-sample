//
//  ShoppingListViewModelTests.swift
//  AholdSampleTests
//
//  Created by Wouter Wisse on 03/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import XCTest
@testable import AholdSample

class ShoppingListViewModelTests: XCTestCase {
    
    var shoppingListViewModel: ShoppingListViewModel!
    
    override func setUp() {
        super.setUp()
        
        let path = Bundle.main.path(forResource: "users", ofType: "json")!
        let data = try! Data(contentsOf: URL(fileURLWithPath: path))
        
        let jsonDataService: JsonDataService<UserList> = JsonDataService(data: data)
        
        let userListViewModel = UserListViewModel(jsonDataService: jsonDataService)
        userListViewModel.loadData()
        
        let cellViewModel = userListViewModel.cellViewModel(at: IndexPath(row: 0, section: 0))
        self.shoppingListViewModel = ShoppingListViewModel(items: cellViewModel.shoppingList)
    }
    
    func testShoppingListViewModelEmptyState() {
        XCTAssertTrue(self.shoppingListViewModel.numberOfRows == 0, "ViewModel should not contain any rows yet.")
    }
    
    func testShoppingListViewModelLoadingData() {
        self.shoppingListViewModel.loadData()
        XCTAssertTrue(self.shoppingListViewModel.numberOfRows == 3, "ViewModel should contain 3 rows.")
    }
    
    func testShoppingListCellViewModelTotalPriceCalculation() {
        let items: [Item] = [
            Item(product: "Milk", amount: 2, price: 1.30),
            Item(product: "Cheese", amount: 1, price: 4.15),
            Item(product: "Butter", amount: 1, price: 0.99)
        ]
        
        let shoppingListViewModel = ShoppingListViewModel(items: items)
        shoppingListViewModel.loadData()
        
        let currencyFormatter = shoppingListViewModel.currencyFormatter
        
        let firstCellViewModel = shoppingListViewModel.cellViewModel(at: IndexPath(row: 0, section: 0))
        let milk = items[0]
        let milkTotalPrice = Double(milk.amount) * milk.price
        let milkTotalPriceString = currencyFormatter.string(from: NSNumber(value: milkTotalPrice))!
        
        XCTAssertTrue(firstCellViewModel.totalPrice == milkTotalPriceString, "Total price string should be equal to the local calculation and formatting.")
    }
}
