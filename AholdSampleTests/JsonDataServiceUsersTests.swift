//
//  JsonDataServiceUsersTests.swift
//  AholdSampleTests
//
//  Created by Wouter Wisse on 03/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import XCTest
@testable import AholdSample

class JsonDataServiceUsersTests: XCTestCase {
    
    var jsonDataService: JsonDataService<UserList>!
    
    override func setUp() {
        super.setUp()
        
        let path = Bundle.main.path(forResource: "users", ofType: "json")!
        let data = try! Data(contentsOf: URL(fileURLWithPath: path))
        self.jsonDataService = JsonDataService(data: data)
    }
    
    func testUserListNotEmpty() {
        do {
            let userList: UserList = try self.jsonDataService.loadData()
            XCTAssertFalse(userList.users.isEmpty, "User List should not be empty.")
        } catch {
            XCTFail("User List could not be parsed.")
        }
    }
    
    func testUserListContainsThreeUsers() {
        do {
            let userList: UserList = try self.jsonDataService.loadData()
            XCTAssertTrue(userList.users.count == 3, "User List should contain 3 users.")
        } catch {
            XCTFail("User List could not be parsed.")
        }
    }
}
