//
//  UserListViewModelTests.swift
//  AholdSampleTests
//
//  Created by Wouter Wisse on 03/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import XCTest
@testable import AholdSample

class UserListViewModelTests: XCTestCase {
    
    var userListViewModel: UserListViewModel!
    
    override func setUp() {
        super.setUp()
        
        let path = Bundle.main.path(forResource: "users", ofType: "json")!
        let data = try! Data(contentsOf: URL(fileURLWithPath: path))
        let jsonDataService: JsonDataService<UserList> = JsonDataService(data: data)
        
        self.userListViewModel = UserListViewModel(jsonDataService: jsonDataService)
        self.userListViewModel.loadData()
    }

    func testUserListViewModelContainsThreeRows() {
        XCTAssertTrue(self.userListViewModel.numberOfRows == 3, "ViewModel should contain 3 rows.")
    }
    
    func testUserListViewModelCellViewModels() {
        for row in 0...2 {
            let indexPath = IndexPath(row: row, section: 0)
            XCTAssertNotNil(self.userListViewModel.cellViewModel(at: indexPath), "ViewModel should have created a CellViewModel for indexPath: \(indexPath)")
        }
    }
}
