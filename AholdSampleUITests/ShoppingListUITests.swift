//
//  AholdSampleUITests.swift
//  AholdSampleUITests
//
//  Created by Wouter Wisse on 03/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import XCTest

class AholdSampleUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        XCUIApplication().launch()
    }
    
    func testFirstUserShoppingListAvailability() {
        let app = XCUIApplication()
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Wouter Wisse"]/*[[".cells.staticTexts[\"Wouter Wisse\"]",".staticTexts[\"Wouter Wisse\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Beer"]/*[[".cells.staticTexts[\"Beer\"]",".staticTexts[\"Beer\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Sunscreen"]/*[[".cells.staticTexts[\"Sunscreen\"]",".staticTexts[\"Sunscreen\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Aftersun"]/*[[".cells.staticTexts[\"Aftersun\"]",".staticTexts[\"Aftersun\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.navigationBars["Shoppinglist"].buttons["Shoppers"].tap()
    }
}
