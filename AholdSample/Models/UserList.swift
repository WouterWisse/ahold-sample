//
//  UserList.swift
//  AholdSample
//
//  Created by Wouter Wisse on 03/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import Foundation

fileprivate extension UserList {
    
    enum CodingKeys: String, CodingKey {
        case users
    }
}

final internal class UserList: Decodable {
    
    // MARK: - Properties
    
    internal var users: [User]
    
    // MARK: - Initializers
    
    init(users: [User]) {
        self.users = users
    }
}
