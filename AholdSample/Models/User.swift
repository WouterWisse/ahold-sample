//
//  User.swift
//  AholdSample
//
//  Created by Wouter Wisse on 03/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import Foundation

internal extension User {
    
    enum CodingKeys: String, CodingKey {
        case firstName
        case lastName
        case favoriteStore
        case shoppingList
    }
}

final internal class User: Decodable {
    
    // MARK: - Internal Properties
    
    internal var firstName: String
    internal var lastName: String
    internal var favoriteStore: String?
    internal var shoppingList: [Item]
    
    // MARK: - Initializers
    
    init(firstName: String, lastName: String, favoriteStore: String?, shoppingList: [Item]) {
        self.firstName = firstName
        self.lastName = lastName
        self.favoriteStore = favoriteStore
        self.shoppingList = shoppingList
    }
}
