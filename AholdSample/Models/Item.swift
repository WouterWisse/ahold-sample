//
//  Item.swift
//  AholdSample
//
//  Created by Wouter Wisse on 03/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import Foundation

internal extension Item {
    
    enum CodingKeys: String, CodingKey {
        case product
        case amount
        case price
    }
}

final internal class Item: Decodable {
    
    // MARK: - Properties
    
    internal var product: String
    internal var amount: Int
    internal var price: Double
    
    // MARK: - Initializers
    
    init(product: String, amount: Int, price: Double) {
        self.product = product
        self.amount = amount
        self.price = price
    }
}
