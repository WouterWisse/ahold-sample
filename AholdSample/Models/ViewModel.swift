//
//  ViewModel.swift
//  AholdSample
//
//  Created by Wouter Wisse on 03/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import Foundation

internal protocol ViewModel {
    typealias DataDidChangeClosure = (()->())?
    associatedtype CellViewModel: CellViewModelProtocol
    
    var numberOfRows: Int { get }
    var numberOfSections: Int { get }
    func loadData()
    func cellViewModel(at indexPath: IndexPath) -> CellViewModel
}

internal protocol CellViewModelProtocol {}


