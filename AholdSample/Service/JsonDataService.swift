//
//  JsonDataService.swift
//  AholdSample
//
//  Created by Wouter Wisse on 03/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import Foundation

internal enum JsonDataServiceError: Error {
    case unknownError
    case noDataError
    case decodingError(description: String)
}

internal class JsonDataService<T: Decodable> {
    
    // MARK: - Private Properties
    
    private var data: Data?
    
    // MARK: - Initialize
    
    init(data: Data? = nil) {
        self.data = data
    }
    
    // MARK: - Internal Functions
    
    func loadData() throws -> T  {
        guard let data = self.data else { throw JsonDataServiceError.noDataError }
        
        let decoder = JSONDecoder()
        do {
            let users = try decoder.decode(T.self, from: data)
            return users
        } catch {
            throw JsonDataServiceError.decodingError(description: error.localizedDescription)
        }
    }
}
