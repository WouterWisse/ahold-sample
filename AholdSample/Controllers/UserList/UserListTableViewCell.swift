//
//  UserListTableViewCell.swift
//  AholdSample
//
//  Created by Wouter Wisse on 03/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import UIKit

final internal class UserListTableViewCell: UITableViewCell {
    
    static let reuseIdentifier = "UserListTableViewCellIdentifier"
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var favoriteStoreLabel: UILabel!
    @IBOutlet weak var shoppingListLabel: UILabel!
}
