//
//  UserListTableViewController.swift
//  AholdSample
//
//  Created by Wouter Wisse on 03/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import UIKit

final internal class UserListTableViewController: UITableViewController {
    
    // MARK: - Private Properties
    
    /* Normally you would want to pass this controller some data to make it more loosely coupled,
     instead of initializing the data here. */
    
    private lazy var viewModel: UserListViewModel = {
        let path = Bundle.main.path(forResource: "users", ofType: "json")!
        let data = try! Data(contentsOf: URL(fileURLWithPath: path))
        return UserListViewModel(jsonDataService: JsonDataService(data: data))
    }()
    
    
    // MARK: - ViewController Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initViewModel()
    }
    
    // MARK: - Private Functions
    
    private func initViewModel() {
        self.viewModel.dataDidChangeClosure = { [weak self] in
            self?.tableView.reloadData()
        }
        
        self.viewModel.loadData()
    }
}

// MARK: - UITableView DataSource

internal extension UserListTableViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.numberOfSections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfRows
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UserListTableViewCell.reuseIdentifier, for: indexPath) as! UserListTableViewCell
        let cellViewModel: UserListCellViewModel = self.viewModel.cellViewModel(at: indexPath)
        
        cell.nameLabel.text = cellViewModel.fullName
        cell.favoriteStoreLabel.text = cellViewModel.favoriteStore
        cell.shoppingListLabel.text = cellViewModel.shoppingListDescription
        
        return cell
    }
}

// MARK: - UITableView Delegate

internal extension UserListTableViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellViewModel: UserListCellViewModel = self.viewModel.cellViewModel(at: indexPath)
        if let shoppingListController = ShoppingListTableViewController.instantiateFromStoryboard(with: cellViewModel.shoppingList) {
            self.navigationController?.pushViewController(shoppingListController, animated: true)
        }
    }
}
