//
//  UserListViewModel.swift
//  AholdSample
//
//  Created by Wouter Wisse on 03/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import Foundation

internal class UserListViewModel: ViewModel {
    typealias CellViewModel = UserListCellViewModel
    
    // MARK: - Internal Properties
    
    internal var dataDidChangeClosure: DataDidChangeClosure
    
    internal var numberOfRows: Int {
        return self.cellViewModels.count
    }
    
    internal var numberOfSections: Int {
        return 1
    }
    
    // MARK: - Private Properties
    
    private var jsonDataService: JsonDataService<UserList>
    
    private var cellViewModels: [UserListCellViewModel] = [UserListCellViewModel]() {
        didSet {
            self.dataDidChangeClosure?()
        }
    }
    
    // MARK: - Initializers
    
    init(jsonDataService: JsonDataService<UserList> = JsonDataService()) {
        self.jsonDataService = jsonDataService
    }
    
    // MARK: - Internal Functions
    
    internal func loadData() {
        do {
            let userList = try self.jsonDataService.loadData()
            self.createCellViewModels(for: userList)
        } catch {
            print("Error in fetching users: \(error.localizedDescription)")
        }
    }
    
    internal func cellViewModel(at indexPath: IndexPath) -> UserListCellViewModel {
        return self.cellViewModels[indexPath.row]
    }
    
    // MARK: - Private Functions
    
    private func createCellViewModels(for userList: UserList) {
        var cellViewModels = [UserListCellViewModel]()
        
        for user in userList.users {
            let fullName = user.firstName + " " + user.lastName
            let favoriteStore = user.favoriteStore ?? "No favorite store"
            let shoppingListDescription = "Groceries: \(user.shoppingList.count)"
            
            let userListCellViewModel = UserListCellViewModel(fullName: fullName,
                                                              favoriteStore: favoriteStore,
                                                              shoppingListDescription: shoppingListDescription,
                                                              shoppingList: user.shoppingList)
            cellViewModels.append(userListCellViewModel)
        }
        
        self.cellViewModels = cellViewModels
    }
}

// MARK: - UserListCellViewModel

internal struct UserListCellViewModel: CellViewModelProtocol {
    var fullName: String
    var favoriteStore: String
    var shoppingListDescription: String
    var shoppingList: [Item]
}
