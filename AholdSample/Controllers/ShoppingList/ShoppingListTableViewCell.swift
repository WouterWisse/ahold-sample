//
//  ShoppingListTableViewCell.swift
//  AholdSample
//
//  Created by Wouter Wisse on 03/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import UIKit

final internal class ShoppingListTableViewCell: UITableViewCell {
    
    static let reuseIdentifier = "ShoppingListTableViewCellIdentifier"
    
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
}
