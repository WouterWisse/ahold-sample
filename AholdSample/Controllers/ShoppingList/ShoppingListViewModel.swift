//
//  ShoppingListViewModel.swift
//  AholdSample
//
//  Created by Wouter Wisse on 03/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import Foundation

internal class ShoppingListViewModel: ViewModel {
    
    typealias CellViewModel = ShoppingListCellViewModel
    
    // MARK: - Internal Properties
    
    internal var dataDidChangeClosure: DataDidChangeClosure
    
    internal var numberOfRows: Int {
        return self.cellViewModels.count
    }
    
    internal var numberOfSections: Int {
        return 1
    }
    
    internal var titleForHeader: String {
        return "GROCERIES: \(self.numberOfRows)"
    }
    
    internal var titleForFooter: String {
        let totalPerProduct = self.items.map { Double($0.amount) * $0.price }
        let totalPrice = totalPerProduct.reduce(0, +)
        let totalPriceString = self.currencyFormatter.string(from: NSNumber(value: totalPrice))
        return "TOTAL: " + (totalPriceString ?? "-")
    }
    
    // MARK: - Private Properties
    
    private var items: [Item]
    
    private var cellViewModels: [ShoppingListCellViewModel] = [ShoppingListCellViewModel]() {
        didSet {
            self.dataDidChangeClosure?()
        }
    }
    
    private(set) lazy var currencyFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        let locale = Locale.current
        formatter.numberStyle = .currency
        formatter.locale = locale
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        return formatter
    }()
    
    // MARK: - Initializer
    
    init(items: [Item]) {
        self.items = items
    }
    
    // MARK: - Internal Functions
    
    internal func loadData() {
        self.createCellViewModels(for: self.items)
    }
    
    internal func cellViewModel(at indexPath: IndexPath) -> ShoppingListCellViewModel {
        return self.cellViewModels[indexPath.row]
    }
    
     // MARK: - Private Functions
    
    private func createCellViewModels(for items: [Item]) {
        var cellViewModels = [ShoppingListCellViewModel]()
        
        for item in items {
            let productName = item.product
            let details = "\(item.amount) x " + (self.currencyFormatter.string(from: NSNumber(value: item.price)) ?? "-")
            let totalPrice = Double(item.amount) * item.price
            let totalPriceString = self.currencyFormatter.string(from: NSNumber(value: totalPrice)) ?? "-"
            let shoppingListCellViewModel = ShoppingListCellViewModel(productName: productName,
                                                                      details: details,
                                                                      totalPrice: totalPriceString)
            cellViewModels.append(shoppingListCellViewModel)
        }
        
        self.cellViewModels = cellViewModels
    }
}

// MARK: - ShoppingListCellViewModel

internal struct ShoppingListCellViewModel: CellViewModelProtocol {
    var productName: String
    var details: String
    var totalPrice: String
}
