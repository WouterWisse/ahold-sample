//
//  ShoppingListTableViewController.swift
//  AholdSample
//
//  Created by Wouter Wisse on 03/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import UIKit

final internal class ShoppingListTableViewController: UITableViewController {

    // MARK: - Private Properties
    
    private var items: [Item]!
    
    private lazy var viewModel: ShoppingListViewModel = {
        return ShoppingListViewModel(items: self.items)
    }()
    
    // MARK: - Class Functions
    
    internal class func instantiateFromStoryboard(with items: [Item]) -> ShoppingListTableViewController? {
        // Note: would be nice to use some constants here.. https://cocoapods.org/pods/R.swift.
        let storyboard = UIStoryboard(name: "ShoppingList", bundle: nil)
        if let controller = storyboard.instantiateViewController(withIdentifier: "ShoppingListTableViewController") as? ShoppingListTableViewController {
            controller.items = items
            return controller
        }
        
        return nil
    }
    
    // MARK: - ViewController Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initViewModel()
    }
    
    // MARK: - Private Functions
    
    private func initViewModel() {
        self.viewModel.dataDidChangeClosure = { [weak self] in
            self?.tableView.reloadData()
        }
        
        self.viewModel.loadData()
    }
}

// MARK: - UITableView DataSource

internal extension ShoppingListTableViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.numberOfSections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfRows
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.viewModel.titleForHeader
    }
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return self.viewModel.titleForFooter
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ShoppingListTableViewCell.reuseIdentifier, for: indexPath) as! ShoppingListTableViewCell
        let cellViewModel = self.viewModel.cellViewModel(at: indexPath)
        
        cell.productNameLabel.text = cellViewModel.productName
        cell.detailsLabel.text = cellViewModel.details
        cell.totalPriceLabel.text = cellViewModel.totalPrice
                
        return cell
    }
}
